#!/bin/bash

echo "Hello!, This script will run the examples provided in Robot.md"

# Run the commands provided in the instructional document
echo
echo
echo
echo  "EXAMPLE 1:"

node ./src/run.js <<STDIN
PLACE 0,0,NORTH
MOVE
REPORT
STDIN

echo
echo
echo
echo  "EXAMPLE 2:"

node ./src/run.js <<STDIN
PLACE 0,0,NORTH
LEFT
REPORT
STDIN


echo
echo
echo
echo  "EXAMPLE 3:"

node ./src/run.js <<STDIN
PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT
STDIN


echo
echo
echo "I look forward to hearing your feedback :)"
echo "Reza Bandegi"
echo "rbandegi@gmail.com"
echo
echo
