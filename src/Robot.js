/**
 * @author Reza Bandegi
 */

/* jshint esversion: 6 */
/* globals  Promise, console */

const _ = require("lodash");


/**
 * Example Lang Object
 * @type {Object}
 */
// const lang = {
//   heading : {
//     EAST : 'EAST',
//     WEST : 'WEST',
//     SOUTH : 'SOUTH',
//     NORTH : 'NORTH',
//   },
//   direction : {
//     LEFT    : 'LEFT',
//     RIGHT  : 'RIGHT',
//   },
//   action : {
//     MOVE : "MOVE",
//     REPORT : "REPORT",
//     PLACE : "PLACE",
//   }
// };


/**
 * Robot Class
 * @class
 * @classdesc This class accepts commands, parses, verifies and moves the robot
 *
 */
var Robot = class Robot {


  /**
   * @param {Object} options         - Options Object
   * @param {Number} options.yMax    - Y-dimenstions of the board
   * @param {Number} options.xMax    - X-dimenstions of the board
   * @param {Object} options.lang    - See above for Example Lang Object
   */
  constructor(options) {
    if( !_.isObject(options)      ||
        isNaN(options.yMax)       ||
        isNaN(options.xMax)       ||
        !_.isObject(options.lang)
      ){
      throw new Error("Invalid Arguments");
    }


    if(options.yMax < 0 || options.yMin === Infinity || options.xMax < 0 || options.xMin === Infinity){
      throw new Error("Invalid Arguments");
    }

    this.board = {
      x : parseInt(options.xMax),
      y : parseInt(options.yMax)
    };
    this.currentPos = {
      x : null,
      y : null,
      heading : null,
    };
    this.lang = options.lang;
    this.isPlaced = false;
    this._heading = [
        {
          key : "NORTH",label: this.lang.heading.NORTH, x:  0, y: 1, RIGHT: 'EAST',  LEFT: 'WEST'
        },
        {
          key : "EAST",label: this.lang.heading.EAST,  x:  1, y:  0, RIGHT: 'SOUTH', LEFT: 'NORTH'
        },
        {
          key : "SOUTH",label: this.lang.heading.SOUTH, x:  0, y:  -1, RIGHT: 'WEST',  LEFT: 'EAST'
        },
        {
          key : "WEST",label: this.lang.heading.WEST,  x: -1, y:  0, RIGHT: 'NORTH', LEFT: 'SOUTH'
        }
    ];
    this._direction = {
      LEFT : this.lang.direction.LEFT,
      RIGHT : this.lang.direction.RIGHT,
    };
  }


  /**
   * This function uses _.inRange to determine if the board is in position
   * @param {Object} posObj      - Position Object
   * @param {Number} posObj.x    - X Position
   * @param {Number} posObj.y    - Y Position
   */
  isOnTable(posObj){
    return _.inRange(posObj.x, 0,this.board.x+1) && _.inRange(posObj.y, 0,this.board.y+1);
  }


  /**
   * Given a heading, find the corresponding Heading Object from this._heading
   * @param   {String} headingLabel - the label we are looking for
   * @return  {Object|Undefined}
   */
  getHeadingFromLabel(headingLabel){
    return _.find(this._heading,{label : headingLabel});
  }

  /**
   * Same as above, but using key as the query
   * @param {String} key - the label we are looking for
   * @return  {Object|Undefined}
   */
  getHeadingFromKey(key){
    return _.find(this._heading,{key : key});
  }

  /**
   * Attempts to move the robot in the direction of the heading
   * @return {Object} this.currentPos
   * @return {Boolean} False if robot didn't move
   */
  move(){
    if(!this.isPlaced){
      return false;
    }
    let headingObj = this.getHeadingFromKey(this.currentPos.heading);
    let newPos = {
      x : (this.currentPos.x || 0) + headingObj.x,
      y : (this.currentPos.y || 0) + headingObj.y
    };

    if(this.isOnTable(newPos)){
      this.currentPos.x = newPos.x;
      this.currentPos.y = newPos.y;
      return this.currentPos;
    }else{
      //console.trace("ignored");
      return false;
    }
  }

  /**
   * Rotate the robot
   * @param {String} direction - The direction we are moving in
   */
  rotate(direction){
    if(!this.isPlaced){
      return false;
    }
    let directionKey = this._direction[direction];
    if(_.isUndefined(directionKey)){
      throw new Error("Invalid Direction");
    }
    let headingObj = this.getHeadingFromKey(this.currentPos.heading);
    this.currentPos.heading = headingObj[directionKey];
    return true;
  }

  /**
   * Print the current position
   */
  report(){
    if(this.isPlaced){
      console.log("Output:  "+this.currentPos.x+","+this.currentPos.y+","+this.currentPos.heading);
      return true;
    }else{
      console.log("Robot is not placed");
      return false;
    }
  }

  /**
   * Validate a given Position Object
   * @param {Object} posObj           - Position Object
   * @param {Number} posObj.x         - X Position
   * @param {Number} posObj.y         - Y Position
   * @param {String} posObj.heading   - heading label
   * @return {Object} posObject
   */
  validatePosObj(posObj){
    if(_.isUndefined(posObj)){
      throw new Error("Invalid Position Object");
    }
    var x = Number(posObj.x);
    var y = Number(posObj.y);
    var heading = this.getHeadingFromLabel(posObj.heading);
    if(isNaN(x) || isNaN(y) || typeof heading === "undefined"){
      throw new Error("Invalid Position Object");
    }
    return {
      x : x,
      y : y,
      heading : posObj.heading,
    };
  }


  /**
   * Place a robot on a given position
   * @param {Number} posObj.x         - X Position
   * @param {Number} posObj.y         - Y Position
   * @param {String} posObj.heading   - heading label
   */
  place(posObj){
    posObj = this.validatePosObj(posObj);
    let headingObj = this.getHeadingFromLabel(posObj.heading);
    let isOnTable = this.isOnTable(posObj);
    if(isOnTable && _.isObject(headingObj)){
      this.isPlaced = true;
      this.currentPos.x = posObj.x;
      this.currentPos.y = posObj.y;
      this.currentPos.heading = headingObj.key;
      return true;
    }else{
      //console.trace("Ignored");
      return false;
    }
  }
};

//
// var r = new Robot({
//   lang : {
//     heading : {
//       EAST : "EAST",
//       WEST : "WEST",
//       SOUTH : "SOUTH",
//       NORTH : "NORTH",
//     },
//     direction : {
//       LEFT    : "LEFT",
//       RIGHT  : "RIGHT",
//     }
//   },
//   yMax : 5, xMax : 5
// });
//
//
// r.report();



module.exports = Robot;
