/**
 * @author Reza Bandegi
 */

/*jshint esversion: 6 */
/* globals  Promise, console */



const readline      = require('readline');
const EventEmitter  = require('events').EventEmitter;
const _             = require("lodash");


/**
 * This class reads the user input and parses the user command
 * @class
 * @classdesc This class reads the user input and parses the user command
 * It also checks to see if the valid init command has been sent.
 * Checks to see if the command sent is valid
 * Checks the argument is valid
 */
var LineReader = class LineReader {


  /**
   * Constructor
   * @param {Object} options              - Options
   * @param {string} options.initCommand  - The init command required for the line reader to start accepting commands
   * @param {Array} options.validCommands- Array of commands that are accepted
   */
  constructor(options) {
    if(!_.isObject(options) || !_.isArray(options.validCommands) || !_.isString(options.initCommand)){
      throw new Error("Invalid Arguments");
    }
    this.initValidCommand = options.initCommand;
    this.validCommands    = _.map(options.validCommands,"command");
    this.commandArgs      = options.validCommands;
    this.hasInitVal       = false;
    this.rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout
    });
  }



  /**
   * askQuestion:
   * Ask a Question from a user
   * @param   {String} question       - The Question the class will ask
   * @returns {Promise} promise       - Resolve : Valid Command and the init cammand has been passed
   *                                  - Reject : Invalid Command or the init command has not been passed
   */
  askQuestion(question){
    if(!_.isString(question)){
      throw new Error("Question must be a string");
    }
    let addLine = (line)=>{
      this.parseAnswer(line,
      (obj)=>{
        lines.push(obj);
      },
      (obj)=>{

      });

      this.rl.question("", addLine);
    };
    var lines = [];
    return new Promise((res,rej)=>{
      this.rl.question(question, addLine);
      this.rl.once('pause',()=>{
        res(lines);
      });
    });
  }


  parseAnswer(answer,res,rej){

    var validCommand = this.isValidCommand(answer);
    if(validCommand === false){
      return rej({
        answer : answer,
        isInited : this.hasInitVal,
      });
    }

    var isInit = this.isInitCommand(validCommand);

    var commandArgs = this.getCommandArgs(validCommand,answer);
    var isValidArgs = this.isValidArgs(validCommand,commandArgs);


    if( _.isString(validCommand) && isValidArgs === true){
      if(!this.hasInitVal && isInit){
        this.hasInitVal = true;
      }

      res({
        command     : validCommand,
        isInited    : this.hasInitVal,
        commandArgs : commandArgs
      });

    }else{
      rej({
        answer    : answer,
        isInited  : this.hasInitVal,
      });
    }
  }




  /**
   * Get the the command argumnets
   * @param   {String} command  - The valid command
   * @param   {String} str      - Unparsed answer
   * @return  {String} str      - Cleaned up argument string
   */
  getCommandArgs(command,str){
    return _.trimEnd(_.trimStart(str.replace(command,"")));
  }




  /**
   * @param {String} command  - The command we are checking the argument for
   * @param {String} arg      - The args for the commmand
   */
  isValidArgs(command,arg){
    var args = _.find(this.commandArgs,{command : command});
    if(args === "undefined"){
      return true;
    }

    if(_.isRegExp(args.regex)){
      return args.regex.test(arg);
    }

    return true;
  }




  /**
   * @param   {string} command is the command the init comand?
   * @return  {Boolean}       True : the command IS the initCommand | False: the command is NOT the init command
   */
  isInitCommand(command){
    if(this.initValidCommand === command){
      return true;
    }
    return false;
  }




  /**
   * @param {String} str The command string is a valid command
   * @return {String|Boolean} False : The command string is not valid | return the valid command string
   */
  isValidCommand(str){
    var split = str.split(" ");
    if(split.length < 1){
      return false;
    }
    var validArr = _.filter(this.validCommands,(el)=>{
      return el === split[0];
    });
    if(validArr.length === 0){
      return false;
    }
    return validArr[0];
  }




  /**
   * User has ended the input session
   *
   * @fires LineReader#end
   * @listens readline:pause
   */
  _onEnd(){
    /**
     * End Event
     *
     * @event Hurl#end
     * @type {String}
     */
    console.log("End");
  }
};


// var l = new LineReader({
//   initCommand : "PLACE",
//   validCommands : [
//     {
//       command : "PLACE",
//       regex : /(\d+)\s(\d+)\s((NORTH)|(SOUTH)|(EAST)|(WEST))/
//     }
//   ],
// });
//
// var ask = function(){
//   l.askQuestion("Hello\n")
//   .then(function(answer){
//     console.log(answer);
//   }).catch(()=>{
//     console.log("ERR");
//     ask();
//   });
// };
// ask();
//
// l.on("end",()=>{
//   console.log("end");
// });

module.exports = LineReader;
