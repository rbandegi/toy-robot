/**
 * @author Reza Bandegi
 */

/* jshint esversion: 6 */

const Robot = require('./Robot.js');
const LineReader = require('./LineReader.js');

/**
 * Strings
 */
 const lang = {
   heading : {
     EAST : 'EAST',
     WEST : 'WEST',
     SOUTH : 'SOUTH',
     NORTH : 'NORTH',
   },
   direction : {
     LEFT    : 'LEFT',
     RIGHT  : 'RIGHT',
   },
   action : {
     MOVE : "MOVE",
     REPORT : "REPORT",
     PLACE : "PLACE",
   }
 };




/**
 * Create a Class of LineReader, to accept and parse input
 * @type {LineReader}
 */
 let lr = new LineReader({
   initCommand : lang.action.PLACE,
   validCommands : [
     {
       command : lang.action.PLACE,
       regex : RegExp('(\\d+)(,)(\\d+)(,)(('+lang.heading.NORTH+')|('+lang.heading.SOUTH+')|('+lang.heading.EAST+')|('+lang.heading.WEST+'))')
     },
     {
       command : lang.action.MOVE
     },
     {
       command : lang.direction.LEFT
     },
     {
       command : lang.direction.RIGHT
     },
     {
       command : lang.action.REPORT
     }
   ],
 });



/**
 * Create a Class of robot to move the robot
 * @type {Robot}
 */
let robot = new Robot({
  lang : lang,
  yMax : 5,
  xMax : 5
});



/**
 * Send an array of commands to the robot
 * @param  {Array} lines  Each element of the array is a command for the robot
 */
let runCommand = function(lines){
  lines.forEach(function(line){
    if(line.command === lang.action.PLACE){
      let obj = line.commandArgs.split(",");
      robot.place({
        x : obj[0],
        y : obj[1],
        heading : obj[2]
      });
    }else if (line.command === lang.action.REPORT) {
      robot.report();
    }else if ( line.command === lang.action.MOVE) {
      robot.move();
    }else if (line.command === lang.direction.LEFT || line.command === lang.direction.RIGHT) {
      robot.rotate(line.command);
    }else{
      console.log("Invalid Command");
    }
  });
};


/**
 * Start the program by asking a user a question
 */
var askUser = function(){
  console.log("\n\nToy Robot Simulator \n\n");
  console.log("Every command must be in a new line");
  console.log("Press Ctrl+C on a new line to run the commands\n");
  console.log("Example Input :");
  console.log("PLACE 0,0,NORTH");
  console.log("MOVE");
  console.log("REPORT");
  console.log("Ctrl+C");
  console.log("--------------------------------------------------\n");
  lr.askQuestion("Enter Command:\n\n")
  .then(runCommand)
  .catch(()=>{
    askUser();
  });
};
askUser();
