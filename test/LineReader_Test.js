const chai    = require('chai');
const expect  = chai.expect;

const LineReader = require('../src/LineReader.js');

var LR = function(){
  /**
   * Strings
   */
   const lang = {
     heading : {
       EAST : 'EAST',
       WEST : 'WEST',
       SOUTH : 'SOUTH',
       NORTH : 'NORTH',
     },
     direction : {
       LEFT    : 'LEFT',
       RIGHT  : 'RIGHT',
     }
   };

   return new LineReader({
     initCommand : 'PLACE',
     validCommands : [
       {
         command : 'PLACE',
         regex : RegExp('(\\d+)\\s(\\d+)\\s(('+lang.heading.NORTH+')|('+lang.heading.SOUTH+')|('+lang.heading.EAST+')|('+lang.heading.WEST+'))')
       },
       { command : 'MOVE'},
       { command : 'LEFT'},
       { command : 'RIGHT'},
       { command : 'REPORT'}
     ],
   });

};



describe('LineReader', function() {
  
  describe('#constructor()', function() {


    it('should throw error when no options is passed', function() {
      var fn = function(){
        return new LineReader();
      };
      expect(fn).to.throw('Invalid Arguments');
    });


    it('should throw error when invalid options are passed', function() {
      var fn = function(){
        return new LineReader({
          validCommands : {},
          initCommand : 'TEST'
        });
      };
      expect(fn).to.throw('Invalid Arguments');
    });


    it('should return instance of Linereader when valid options are passed', function() {
      expect(LR()).to.have.instanceof(LineReader);
    });
  });









  describe('#askQuestion()', function() {


    it('should throw error if no string is passed ', function() {
      var fn = function(){
        var lr = LR();
        lr.askQuestion();
      };
      expect(fn).to.throw('Question must be a string');
    });


    it('should return instance of promise if string is passed ', function() {
      var lr = LR();
      var g =  lr.askQuestion('\nTest Question\n');
      expect(g).to.have.instanceof(Promise);
    });
  });


  describe('#parseAnswer()', function() {

    it('should reject promise if invalid command is sent ', function() {
      var lr = LR();
      var answer = 'BAD';
      var p =  lr.parseAnswer(answer,
      function res(arg){
        expect(arg.command).to.eql(answer);
      }, function rej(arg){
        expect(arg.answer).to.eql(answer);
      });
    });




    it('should reject promise if correct command with bad args is sent ', function() {
      var lr = LR();
      var answer = 'PLACE bad command';
      var p =  lr.parseAnswer(answer,
      function res(arg){
        expect(arg.command).to.eql(answer);
      }, function rej(arg){
        expect(arg.answer).to.eql(answer);
      });
    });




    it('should reject promise if correct command with args with bad syntax is sent ', function() {
      var lr = LR();
      var answer = 'PLACE 55 WEST';
      var p =  lr.parseAnswer(answer,
      function res(arg){
        expect(arg.command).to.eql(answer);
      }, function rej(arg){
        expect(arg.answer).to.eql(answer);
      });
    });




    it('should reject promise if correct command with bad args is sent with no space seperating ', function() {
      var lr = LR();
      var answer = 'PLACEbadcommand';
      var p =  lr.parseAnswer(answer,
      function res(arg){
        expect(arg.command).to.eql(answer);
      }, function rej(arg){
        expect(arg.answer).to.eql(answer);
      });
    });




    it('should reject promise if correct command is sent before the init command ', function() {
      var lr = LR();
      var answer = 'REPORT';
      var p =  lr.parseAnswer(answer,
      function res(arg){
        expect(arg.command).to.eql(answer);
      }, function rej(arg){
        expect(arg.answer).to.eql(answer);
      });
    });




    it('should accept promise if correct command with correct args is sent ', function() {
      var lr = LR();
      var answer = 'PLACE 5 5 EAST';
      var p =  lr.parseAnswer(answer,
      function res(arg){
        expect(arg.command).to.eql('PLACE');
        expect(arg.commandArgs).to.eql('5 5 EAST');
        expect(arg.isInited).to.eql(true);
      }, function rej(arg){
        throw new Error('Failed ');
      });
    });



  });



});
