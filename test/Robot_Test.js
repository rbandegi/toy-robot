/**
 * @author Reza Bandegi
 */

/* jshint esversion: 6 */
/* globals  Promise, console */

const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;

const _Robot = require('../src/Robot.js');

/**
 * Strings
 */
 const lang = {
   heading : {
     EAST : 'EAST',
     WEST : 'WEST',
     SOUTH : 'SOUTH',
     NORTH : 'NORTH',
   },
   direction : {
     LEFT    : 'LEFT',
     RIGHT  : 'RIGHT',
   },
   action : {
     MOVE : "MOVE",
     REPORT : "REPORT",
     PLACE : "PLACE",
   }
 };
var Robot = function(){

   /**
    * Create a Class of robot to move the
    * @type {Robot}
    */
   return new _Robot({
     lang : lang,
     yMax : 5,
     xMax : 5
   });


};
Robot = Robot();



describe('Robot', function() {


  describe('#constructor()', function() {

    it('should throw error when no options is passed', function() {
      var fn = function(){
        return new _Robot();
      };
      expect(fn).to.throw('Invalid Arguments');
    });


    it('should throw error when invalid options are passed', function() {
      var fn = function(){
        return new _Robot({
          xMin : -1, yMax : 1
        });
      };
      expect(fn).to.throw('Invalid Arguments');
    });


    it('should return instance of Robot when valid options are passed', function() {

      expect(Robot).to.have.instanceof(_Robot);
    });

  });





  describe('#move()', function() {
    Robot.place({
      x : 0, y : 0, heading : lang.heading.NORTH
    });
    it('should return currentPost if robot has moved', function() {
      var pos = Robot.move();
      assert.strictEqual(pos.x,0,"Invalid x position");
      assert.strictEqual(pos.y,1,"Invalid y position");
    });

    it('should return false when robot is not placed', function() {
      let r =  new _Robot({
        lang : lang,
        yMax : 5,
        xMax : 5
      });
      var pos = r.move();
      assert.isFalse(pos,"Robot Position should be false");
    });
    it('should throw error if direction is valid', function() {
      Robot.place({
        x : 5, y : 5,  heading : lang.heading.NORTH
      });
      var pos = Robot.move();
      assert.isFalse(pos,"Robot Position should be false");
    });

  });



  describe('#rotate()', function() {

    it('should throw error if direction is invalid', function() {
      expect(()=>{
        return Robot.rotate("BAD");
      }).to.throw('Invalid Direction');
    });

    it('should throw error if direction is valid', function() {
      expect(()=>{
        return Robot.rotate(lang.direction.left);
      }).to.throw('Invalid Direction');
    });
    it('should return false when Robot is not placed', function() {
      let r =  new _Robot({
        lang : lang,
        yMax : 5,
        xMax : 5
      });
      assert.isFalse(r.rotate(lang.direction.left) ,"Robot should ignore rotate command, when not placed");
    });

  });


  describe('#report()', function() {

    var r = new _Robot({
      lang : lang,
      yMax : 5,
      xMax : 5
    });
    it('should return false if robot is not placed', function() {
      assert.isFalse(r.report(),"Robot shouldn't be on the table !");
    });

    it('should return true if robot is not placed', function() {
      r.place({
        x : 1, y : 5, heading : lang.heading.EAST
      });
      assert.isTrue(r.report(),"Robot shouldn't be on the table !");
    });

  });


  describe('#place()', function() {

    it('should return false when x position is bigger than xMax', function() {
      assert.isFalse(Robot.place({
        x : 6, y : 1, heading : lang.heading.EAST
      }),"Robot shouldn't be on the table !");
    });

    it('should throw Error when x position and y Position is not passed', function() {
      expect(()=>{ return Robot.place();}).to.throw('Invalid Position Object');

    });

    it('should return false when y position is bigger than yMax', function() {
      assert.isFalse(Robot.place({
        x : 1, y : 6, heading : lang.heading.EAST
      }),"Robot shouldn't be on the table !");
    });

    it('should throw Error when invalid heading is passed but positions are correct', function() {
      expect(()=>{
        return Robot.place({
          x : 1, y : 6, heading : "BAD"
        });
      }).to.throw('Invalid Position Object');

    });

    it('should throw Error when no heading is passed', function() {
      expect(()=>{
        return Robot.place({
          x : 1, y : 6,
        });
      }).to.throw('Invalid Position Object');

    });

    it('should return true when y position is equal to table yMax', function() {
      assert.isTrue(Robot.place({
        x : 1, y : 5, heading : lang.heading.EAST
      }),"Robot should be on the table !");
    });

    it('should return true when x position is equal to table xMax', function() {
      assert.isTrue(Robot.place({
        x : 5, y : 1, heading : lang.heading.EAST
      }),"Robot should be on the table !");
    });

    it('should return true when x position and y Position is within xMax and yMax', function() {
      assert.isTrue(Robot.place({
        x : 2, y : 2, heading : lang.heading.EAST
      }),"Robot should be on the table !");
    });

  });

});
