# Toy Robot Simulator
#### Coding challenge for Flippa

#### Install

`npm install .`

#### To Run Examples provided in Robot.md
`./main.sh`

#### To run the application
`npm run-script run`

**OR**
`node ./src/run.js`


#### Example:
```
npm run-script run
PLACE 0,0,NORTH
MOVE
REPORT
ctrl+c
```



#### To run Tests
`npm test`


#### Code Structure
`./src/LineReader.js`
This class reads the user input,parses and validates the user command

`./src/Robot.js`
This class accepts user commands and executes the commands

`./src/run.js`
This is the entry point for the application
